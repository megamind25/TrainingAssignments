﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Day3Assignments
{
    class FeetInches
    {

        public delegate double Conversion(double inches);
        public Conversion convert;

        public void FeetToInches(double feet)
        {
            double inches = feet * 12;
            convert(inches);
            //return inches;
        }

        
            

        
    }
}
