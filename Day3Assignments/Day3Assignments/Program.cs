﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Day3Assignments
{
    class Program
    {
        static void Main(string[] args)
        {

            FeetInches fi = new FeetInches();
            fi.convert = display;
            fi.FeetToInches(100);


            ArithmeticOperators ao = new ArithmeticOperators();
            ao.changeDelegate = displayAnswer;
            Console.WriteLine("Enter the operand: ");
            int operand = int.Parse(Console.ReadLine());
            ao.Add(operand);
            ao.Subtract(operand);
            ao.Multiply(operand);
            ao.Divide(operand);

            string sts = "Bad";

            StringDelegates str = new StringDelegates();

            str.stringDelegate1 += Good;
            str.stringDelegate2 += Morning;

            str.stringDelegate1 += str.stringDelegate2;
            str.stringDelegate1(sts);


            Console.WriteLine("Enter temperature in Fahrenheit: ");
            int far = int.Parse(Console.ReadLine());

            double celsius = (Convert.ToDouble(far - 32)) * 5 / 9;
            Console.WriteLine("The temperature in celsius is: "+celsius);

            Console.WriteLine("Enter the number: ");
            int number = int.Parse(Console.ReadLine());
            Console.WriteLine(ao.ConvertDigits(number));
            Console.WriteLine(ao.ConvertPlaces(number));

            Console.ReadKey();
        }

        public static double display(double inches)
        {
            Console.WriteLine("The value converted to inches is: "+ inches);
            return inches;
        }

        public static int displayAnswer(int answer)
        {
            Console.WriteLine("The answer obtained is : " + answer);
            return 0;
        }

        static void Good(string s)
        {
            

            System.Console.WriteLine("  Good, {0}!", s);
        }

        static void Morning(string s)
        {
            System.Console.WriteLine("  Morning, {0}!", s);
        }
    }
}
