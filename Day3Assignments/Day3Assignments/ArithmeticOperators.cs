﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Day3Assignments
{
    public class ArithmeticOperators
    {
        public delegate int NumberChanger(int n);
        public NumberChanger changeDelegate;

        string[] places = { "", "", "hundred", "", "thousand", "lakh " };

        string[] tens = { "","","twenty", "thirty", "forty", "fifty", "sixty", "seventy", "eighty", "ninety" };

        string[] elev = {"ten","eleven","twelve","thirteen","fourteen","fifteen","sixteen","seventeen","eighteen","nineteen"};

        string[] digits = { "zero", "one", "two",
                        "three", "four", "five",
                        "six", "seven", "eight",
                        "nine" };


        public void Add(int operand)
        {
            int num = 100;
            num += operand;
            Console.WriteLine("\nAddition operation output :");
            changeDelegate(num);
        }

        public void Subtract(int operand)
        {
            int num = 100;
            num -= operand;
            Console.WriteLine("\nSubtraction operation output :");
            changeDelegate(num);
        }

        public void Multiply(int operand)
        {
            int num = 100;
            num *= operand;
            Console.WriteLine("\nMultiplication operation output: ");
            changeDelegate(num);
        }

        public void Divide(int operand)
        {
            int num = 100;
            if (operand == 0)
                Console.WriteLine("\nDivision with zero not possible.");
            else
            {
                num /= operand;
                Console.WriteLine("\nDivision operation output: ");
                changeDelegate(num);
            }
        }

        public int ConvertPlaces(int number)
        {
            string ans = "";
            string num = number.ToString();
            string nam = num;
            int max = -1;
            Console.WriteLine(num.Length);


            if (num.Length > 6)
            {
                Console.WriteLine("Sorry, the number cant be converted");
                return 0;
            }

            else if (num.Length == 6)
                max = 5;

            else if (num.Length == 5)
                max = 4;

            else if (num.Length == 4)
                max = 4;

            else if (num.Length == 3)
                max = 2;

            else if (num.Length == 2)
                max = 1;
            


            for (int i = 0; i < num.Length; i++)
            {
                string s = num[i].ToString();
                int n = int.Parse(s);

                if(max > 1)
                {
                    ans += digits[n] + " " + places[max] + " ";
                    Console.WriteLine(ans);
                }
                
                else if(max == 1 || max == 0)
                {
                    Console.WriteLine("In line");
                    if (tens[n] != "" && max == 1)
                    {
                        ans += tens[n] + " ";
                        Console.WriteLine(ans);
                    }
                    else
                    {
                        if (n == 1)
                        {
                            string s1 = num[i + 1].ToString();
                            int n1 = int.Parse(s);
                            ans += elev[n1];
                            Console.WriteLine(ans);
                            return 0;
                        }

                        else if(n > 1)
                        {
                            ans += digits[n];
                            Console.WriteLine(ans);
                            return 0;
                        }

                    }
                        
                }

                if (max == 4 && (nam.Length == 5 || nam.Length == 4))
                    max = max - 2;
                else if (max == 0)
                    max = 0;
                else
                    max = max - 1;
            }
            return 0;
        }




        public string ConvertDigits(int number)
        {
            string ans = "";
            string num = number.ToString();

            for (int i = 0; i < num.Length; i++)
            {
                string s = num[i].ToString();
                int n = int.Parse(s);
                ans += digits[n] + " ";
            }
            return ans;
        }



    }
}
