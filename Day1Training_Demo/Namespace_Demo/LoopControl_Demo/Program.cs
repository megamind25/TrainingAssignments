﻿using System;
using System.Collections.Generic;

namespace LoopControl_Demo
{
    class Program
    {
        static void Main(string[] args)
        {
            List<string> colors = new List<string> { "White", "green", "red", "blue", "pink" };

            /*
            Console.WriteLine("Looping using foreach loop");
            //looping using foreach
            foreach (string color in colors)
                Console.WriteLine(string.Format(" color name: {0}", color));


            Console.WriteLine("Looping using for loop");
            //looping using for loop
            int colorIndex;
            for (colorIndex = 0; colorIndex < colors.Count; colorIndex++)
                Console.WriteLine(string.Format(" color name: {0}", colors[colorIndex]));
            */
            /*
            Console.WriteLine("Looping using while loop");
            //looping using while loop
            int colorIndex = 0;
            while (colorIndex < colors.Count)
                Console.WriteLine(string.Format(" color name: {0}", colors[colorIndex++]));
            */

            Console.WriteLine("Looping using do while loop");
            //looping using do while loop
            int colorIndex = 0;
            do
            {
                if (colorIndex < colors.Count)
                    Console.WriteLine(string.Format(" color name: {0}", colors[colorIndex]));
            } while (colorIndex++ < colors.Count);

            Console.ReadKey();
        }
    }
}
