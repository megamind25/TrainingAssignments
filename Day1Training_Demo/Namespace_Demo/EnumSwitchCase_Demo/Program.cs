﻿using System;

namespace EnumSwitchCase_Demo
{
    //enum for colors
    enum Color { red, green, blue, pink, yellow, brown }

    class SwitchEnumDemo
    {
        static void Main(string[] args)
        {

            //int? nullableVariable = null;
            //int value = nullableVariable ?? 2;
            //Console.WriteLine(value);

            string message = "Current color is: {0}";
            var randomColorGenerator = new Random();
            for (int iterIndex = 0; iterIndex < 10; iterIndex++)
            {
                Color randomColor = (Color)randomColorGenerator.Next(0, 6);
                switch (randomColor)
                {
                    case Color.blue:
                        Console.WriteLine(string.Format(message, Color.blue));
                        break;

                    case Color.red:
                        Console.WriteLine(string.Format(message, Color.red));
                        break;

                    case Color.pink:
                        Console.WriteLine(string.Format(message, Color.pink));
                        break;

                    case Color.green:
                        Console.WriteLine(string.Format(message, Color.green));
                        break;

                    case Color.yellow:
                        Console.WriteLine(string.Format(message, Color.yellow));
                        break;

                    case Color.brown:
                        Console.WriteLine(string.Format(message, Color.brown));
                        break;
                }
            }
            Console.Read();
        }
    }
}
