﻿using APIDemo.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Web;
using System.Web.Mvc;
using System.Xml;

namespace APIDemo.Controllers
{
    public class ActionController : Controller
    {
        // GET: Action
        public ActionResult Index()
        {
            return View();
        }


        public ActionResult Edit()
        {

            return View();
        }



        public ActionResult Display()
        {
            List<Empl> EmpInfo = new List<Empl>();

            

            using (var client = new HttpClient())
            {

                client.BaseAddress = new Uri("http://localhost:62617");

                var response = client.GetAsync("/api/employees");
                response.Wait();


                var result = response.Result;

                if(result.IsSuccessStatusCode)
                {
                    var readTask = result.Content.ReadAsAsync<List<Empl>>();
                    readTask.Wait();

                    EmpInfo = readTask.Result;
                }

            }

                return View(EmpInfo);
            }


        // GET: Action/Details/5

        public ActionResult Details(int id)
        {





            return View();
        }

        // GET: Action/Create
        public ActionResult Create()
        {
            return View();
        }

        // POST: Action/Create
        [HttpPost]
        public ActionResult Create(Empl NewEmp)
        {
            try
            {
                // TODO: Add insert logic here

                using (var client = new HttpClient())
                {

                    client.BaseAddress = new Uri("http://localhost:62617");

                    var response = client.PostAsJsonAsync<Empl>("/api/employees/",NewEmp);
                    response.Wait();

                    var result = response.Result;

                    if (result.IsSuccessStatusCode)
                    {
                        return RedirectToAction("Display");
                    }


                }

                    return RedirectToAction("Display");
            }
            catch
            {
                return View();
            }
        }



        // GET: Action/Edit/5
        [HttpGet]
        public ActionResult Edit(int id)
        {
            if (id == 0)
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);



            Empl EmpInfo = new Empl();

            using (var client = new HttpClient())
            {

                client.BaseAddress = new Uri("http://localhost:62617");

                var response = client.GetAsync("/api/employees/"+id);
                response.Wait();


                var result = response.Result;

                if (result.IsSuccessStatusCode)
                {
                    var readTask = result.Content.ReadAsAsync<Empl>();
                    readTask.Wait();

                    EmpInfo = readTask.Result;
                }

            }



            return View(EmpInfo);
        }

        // POST: Action/Edit/5
        [HttpPost]
        public ActionResult Edit(int id, Empl EmpChanges)
        {
            using (var client = new HttpClient())
            {

                client.BaseAddress = new Uri("http://localhost:62617");

                //POST
                var putTask = client.PutAsJsonAsync<Empl>("/api/employees/" + id, EmpChanges);
                putTask.Wait();

                var result = putTask.Result;

                if(result.IsSuccessStatusCode)
                {
                    return RedirectToAction("Display");
                }

            }

            return View();
         
        }

        // GET: Action/Delete/5
        public ActionResult DeleteView(int id)
        {
            if (id == 0)
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);



            Empl EmpInfo = new Empl();

            using (var client = new HttpClient())
            {

                client.BaseAddress = new Uri("http://localhost:62617");

                var response = client.GetAsync("/api/employees/" + id);
                response.Wait();


                var result = response.Result;

                if (result.IsSuccessStatusCode)
                {
                    var readTask = result.Content.ReadAsAsync<Empl>();
                    readTask.Wait();

                    EmpInfo = readTask.Result;
                }

            }



            return View(EmpInfo);
        }

        // POST: Action/Delete/5
        [HttpPost]
        public ActionResult DeleteView(int id, FormCollection collection)
        {
            try
            {
                // TODO: Add delete logic here

                if (id == 0)
                    return new HttpStatusCodeResult(HttpStatusCode.BadRequest);



                //Empl EmpInfo = new Empl();

                using (var client = new HttpClient())
                {

                    client.BaseAddress = new Uri("http://localhost:62617");

                    var response = client.DeleteAsync("/api/employees/" + id);
                    response.Wait();



                    return RedirectToAction("Display");

                }
            }
            catch
            {
                return View();
            }
        }
    }
}
