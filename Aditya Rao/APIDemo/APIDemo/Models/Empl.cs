﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace APIDemo.Models
{
    public class Empl
    {
        public int empID { get; set; }
        public string empFirstName { get; set; }
        public string empLastName { get; set; }
        public int empContactNo { get; set; }
        public string empEmail { get; set; }
    }
}