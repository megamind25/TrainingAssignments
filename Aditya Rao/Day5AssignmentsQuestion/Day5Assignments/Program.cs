﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Day5Assignments
{
    class Program
    {
        static void Main(string[] args)
        {



            List<String> SetA = new List<string> {};
            List<String> SetB = new List<string> {};
            List<string> SetC = new List<string> {};
            List<string> SetD = new List<string> {};

            for (int i = 0; i < 10000; i++)
            {
                SetA.Add("A" + i);
                SetB.Add("B" + i);
            }

            for (int i = 0; i < 12000; i++)
            {
                SetC.Add("C" + i);
            }

            for (int i = 0; i < 18000; i++)
            {
                SetD.Add("D" + i);
            }




            Stopwatch sw = Stopwatch.StartNew();


            List<string>[] sets = new List<string>[4];



            sets[0] = SetA;
            sets[1] = SetB;
            sets[2] = SetC;
            sets[3] = SetD;

            List<string> QuestionPaper = new List<string>();

            int SetPointer = 0;
            int[] count = new int[4];
            count[0] = SetA.Count;
            count[1] = SetB.Count;
            count[2] = SetC.Count;
            count[3] = SetD.Count;

            



            int totalCount = SetA.Count + SetB.Count + SetC.Count + SetD.Count;

            int maxQ = Math.Max(Math.Max(SetA.Count, SetB.Count), Math.Max(SetC.Count, SetD.Count));


            for (int i = 0; i < totalCount; i++)
            {
                int max = FindMax(count[0], count[1], count[2], count[3]);
                if(SetPointer != max)
                {
                    SetPointer = max;
                    QuestionPaper.Add(sets[max].ElementAt(sets[max].Count - count[max]));
                    //Console.Write(QuestionPaper.Last()+" ");
                    count[max] = count[max] - 1;

                }

                else
                {
                    if (max == 0)
                        max = FindMax(0, count[1], count[2], count[3]);

                    else if (max == 1)
                        max = FindMax(count[0], 0, count[2], count[3]);

                    else if (max == 2)
                        max = FindMax(count[0], count[1], 0, count[3]);

                    else if (max == 3)
                        max = FindMax(count[0], count[1], count[2], 0);



                    SetPointer = max;
                    QuestionPaper.Add(sets[max].ElementAt(sets[max].Count - count[max]));
                    count[max] = count[max] - 1;

                }


                

                

                


            }

           




            sw.Stop();
            Console.WriteLine("Time taken:" + sw.Elapsed.TotalMilliseconds);
            Console.ReadKey();

        }


        public static int FindMax(int a, int b, int c, int d)
        {
            int max = Math.Max(Math.Max(a, b), Math.Max(c, d));

            if (max == a)
                return 0;
            else if (max == b)
                return 1;
            else if (max == c)
                return 2;
            else if (max == d)
                return 3;

            return 0;
        }
    }
}
