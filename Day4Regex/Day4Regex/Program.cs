﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;

namespace Day4Regex
{
    class Program
    {
        static void Main(string[] args)
        {
            Employee emp = new Employee();
            Regex regex = new Regex(@"^[a-zA-Z0-9]+[a-zA-Z0-9#!$\.]*@[a-z]+\.([a-z]{2,3})$");
            string str;
            Console.WriteLine("Enter mail id");
            str = Console.ReadLine();
            Match match = regex.Match(str);
            if(match.Success)
                Console.WriteLine("Matching");

            Dictionary<int, Employee> employeeDictionary = new Dictionary<int, Employee>();

            Console.Write("Enter number of employees to be added :");
            int number = int.Parse(Console.ReadLine());

            for (int i = 0; i < number; i++)
            {
                emp = emp.AddEmployee();
                employeeDictionary.Add(emp.EmpID, emp);
            }
            ICollection<int> keys = employeeDictionary.Keys;

            for (int i = 0; i < employeeDictionary.Count; i+=2)
            {
                employeeDictionary[keys.ElementAt(i)].Display();
            }



            Console.ReadKey();
        }
    }
}
