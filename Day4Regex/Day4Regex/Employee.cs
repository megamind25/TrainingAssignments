﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Day4Regex
{
    public class Employee
    {
        public int EmpID { get; set; }
        public string Name { get; set; }
        public int Age { get; set; }
        public double Contact { get; set; }

        


        public Employee()
        {

        }

        public Employee(int id, string name, int age, double contact)
        {
            EmpID = id;
            Name = name;
            Age = age;
            Contact = contact;
        }

        public Employee AddEmployee()
        {
            Console.WriteLine("Enter empID");
            int empID = int.Parse(Console.ReadLine());
            Console.WriteLine("Enter name");
            string name = Console.ReadLine();
            Console.WriteLine("Enter age");
            int age = int.Parse(Console.ReadLine());
            Console.WriteLine("Enter contact number");
            double contact = double.Parse(Console.ReadLine());

            Employee emp = new Employee(empID,name,age,contact);

            return emp;
        }

        public void Display()
        {
            Console.WriteLine("Employee ID :"+EmpID);
            Console.WriteLine("Name :"+Name);
            Console.WriteLine("Age :"+Age);
            Console.WriteLine("Contact Number :"+ Contact);
        }


        
    }
}
