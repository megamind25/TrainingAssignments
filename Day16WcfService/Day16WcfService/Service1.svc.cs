﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.ServiceModel;
using System.ServiceModel.Web;
using System.Text;

namespace Day16WcfService


{
    // NOTE: You can use the "Rename" command on the "Refactor" menu to change the class name "Service1" in code, svc and config file together.
    // NOTE: In order to launch WCF Test Client for testing this service, please select Service1.svc or Service1.svc.cs at the Solution Explorer and start debugging.
    public class Service1 : IService1
    {
        List<Employee> EmpList = new List<Employee>();
        //List<Employee> EmpList = new List<Employee>();


        public List<Employee> GetAllEmployees()
        {
            
            Employee emp = new Employee(1, "Aditya");
            EmpList.Add(emp);
            emp = new Employee(2, "Jai");
            EmpList.Add(emp);

            return EmpList;
        }

        public Employee GetEmployeeById(int id)
        {
            Employee emp = new Employee(1, "Aditya");
            EmpList.Add(emp);
            emp = new Employee(2, "Jai");
            EmpList.Add(emp);
            return EmpList.Find(x => x.empID == id);
        }
    }
}
