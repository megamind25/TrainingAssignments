﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.ServiceModel;
using System.ServiceModel.Web;
using System.Text;

namespace Day16WcfService
{
    // NOTE: You can use the "Rename" command on the "Refactor" menu to change the interface name "IService1" in both code and config file together.
    [ServiceContract]
    public interface IService1
    {

        [OperationContract]
        List<Employee> GetAllEmployees();

        [OperationContract]
        Employee GetEmployeeById(int id);

        // TODO: Add your service operations here
    }


    // Use a data contract as illustrated in the sample below to add composite types to service operations.
    [DataContract]
    public class Employee
    {
        //bool boolValue = true;
        //string stringValue = "Hello ";
       


        [DataMember]
        public int empID
        {
            get;
            set;
        }

        [DataMember]
        public string empName
        {
            get;
            set;
        }



        public Employee(int id, string name)
        {
            empID = id;
            empName = name;
        }
    }
}
