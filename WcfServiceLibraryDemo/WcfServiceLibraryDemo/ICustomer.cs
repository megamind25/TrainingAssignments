﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.ServiceModel;
using System.Text;
using System.Threading.Tasks;

namespace WcfServiceLibraryDemo
{
    [ServiceContract]
    interface ICustomer
    {
        [OperationContract(IsOneWay = true)]
        void AddCustomer(CustomerInfo customer);

        [OperationContract]
        List<CustomerInfo> DisplayCustomer();

        [OperationContract]
        CustomerInfo GetCustomer(int id);

        [OperationContract(IsOneWay = true)]
        void DeleteCustomer(int id);

        [OperationContract(IsOneWay = true)]
        void UpdateCustomer(CustomerInfo customer);
    }
}
