﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.ServiceModel;
using System.Text;
using System.Threading.Tasks;
using BusinessLayer;


namespace WcfServiceLibraryDemo
{
    [DataContract]
    public class CustomerInfo
    {
        [DataMember]
        public int CustomerID { get; set; }
        [DataMember]
        public string FirstName { get; set; }
        [DataMember]
        public string LastName { get; set; }
        [DataMember]
        public Nullable<long> ContactNo { get; set; }
    }

    [ServiceBehavior(InstanceContextMode=InstanceContextMode.PerCall,ConcurrencyMode = ConcurrencyMode.Multiple)]
    public class CustomerOperations : ICustomer
    {

        private CustomerInfo TranslateModels(Customer customer)
        {
           CustomerInfo cust = new CustomerInfo();
            cust.CustomerID = customer.CustomerID;
            cust.FirstName = customer.FirstName;
            cust.LastName = customer.LastName;
            cust.ContactNo = customer.ContactNo;

            return cust;
        }


        private Customer TranslateModelsToDB(CustomerInfo customer)
        {
            Customer cust = new Customer();
            cust.CustomerID = customer.CustomerID;
            cust.FirstName = customer.FirstName;
            cust.LastName = customer.LastName;
            cust.ContactNo = customer.ContactNo;

            return cust;
        }


       
        public void AddCustomer(CustomerInfo customer)
        {
            BusinessLogic client = new BusinessLogic();
            client.AddCustomer(TranslateModelsToDB(customer));
        }




        public List<CustomerInfo> DisplayCustomer()
        {

            BusinessLogic client = new BusinessLogic();
            List<CustomerInfo> CustList = new List<CustomerInfo>();

            var customers = client.DisplayAll();
            foreach (var item in customers)
            {
                CustList.Add(TranslateModels(item));
            }
           
            return CustList;


        }

        public CustomerInfo GetCustomer(int id)
        {
            BusinessLogic client = new BusinessLogic();
            return TranslateModels(client.GetCustomer(id));
        
        }

        public void DeleteCustomer(int id)
        {
            BusinessLogic client = new BusinessLogic();
            client.DeleteCustomer(id);
        }


        public void UpdateCustomer(CustomerInfo customer)
        {
            BusinessLogic client = new BusinessLogic();
            var cust = client.UpdateCustomer(TranslateModelsToDB(customer));
            //if(cust==null)

        }
    }
}
