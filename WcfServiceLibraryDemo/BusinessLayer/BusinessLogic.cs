﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.ServiceModel;
using System.Text;
using System.Threading.Tasks;

namespace BusinessLayer
{
    public class BusinessLogic
    {

        public string AddCustomer(Customer customer)
        {
            using (CustomerEntities context = new CustomerEntities())

            {
                
                    var response = context.Customers.Add(customer);
                    if (response == null)
                    {
                        throw new FaultException("Insertion operation failed.");
                    }
                    else
                    {
                         context.SaveChanges();
                         return("Entry inserted successfully");
                    }
                        
                
            }

        }

        public List<Customer> DisplayAll()
        {
            
            using (CustomerEntities context = new CustomerEntities())
            {
                var customers = (from data in context.Customers select data).ToList();
                return customers;
                
            }
        }


        public Customer GetCustomer(int id)
        {

            using (CustomerEntities context = new CustomerEntities())
            {
                var customer = (from data in context.Customers where data.CustomerID==id select data).SingleOrDefault();
                return customer;
            }

        }


        public void DeleteCustomer(int id)
        {
            using (CustomerEntities context = new CustomerEntities())
            {
                Customer customer = new Customer
                {
                    CustomerID = id
                };

                context.Customers.Attach(customer);
                context.Customers.Remove(customer);
                context.SaveChanges();
            }
        }

        public Customer UpdateCustomer(Customer customer)
        {

            using (CustomerEntities context = new CustomerEntities())
            {
                var edit = (from data in context.Customers where customer.CustomerID == data.CustomerID select data).SingleOrDefault();
                if(edit == null)
                {
                    return null;
                }

                context.Entry(edit).CurrentValues.SetValues(customer);
                context.SaveChanges();

                return GetCustomer(customer.CustomerID);

            }
            
        }

    }
}
