﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.ServiceModel;
using System.Text;
using System.Threading.Tasks;

namespace Day17WCFApp
{
    class Program
    {
        static void Main(string[] args)
        {
            CustomerService.CustomerClient client = new CustomerService.CustomerClient();
            List<CustomerService.CustomerInfo> CustomerList = new List<CustomerService.CustomerInfo>();
            CustomerList = client.DisplayCustomer().ToList();
            foreach(CustomerService.CustomerInfo cust in CustomerList)
            {
                Console.WriteLine(cust.CustomerID);
                Console.WriteLine(cust.FirstName + " " + cust.LastName);
                Console.WriteLine(cust.ContactNo);
            }

            try
            {
                Console.WriteLine("Enter id of the customer to be searched");
                int id = int.Parse(Console.ReadLine());
                CustomerService.CustomerInfo cust1 = client.GetCustomer(id);
                Console.WriteLine(cust1.CustomerID + " " + cust1.FirstName + " " + cust1.LastName + " " + cust1.ContactNo);
            }

            catch(FaultException ex)
            {
                Console.WriteLine(ex.Message);
                Console.ReadLine();
            }
               
               
            

         

            CustomerService.CustomerInfo customer = new CustomerService.CustomerInfo();
            Console.WriteLine("Enter new customer details");
            Console.WriteLine("Enter id");
            customer.CustomerID = int.Parse(Console.ReadLine());
            Console.WriteLine("Enter first name");
            customer.FirstName = Console.ReadLine();
            Console.WriteLine("Enter last name");
            customer.LastName = Console.ReadLine();
            Console.WriteLine("Enter contact number");
            customer.ContactNo = long.Parse(Console.ReadLine());

            client.AddCustomer(customer);


            CustomerList = client.DisplayCustomer().ToList();
            foreach (CustomerService.CustomerInfo cust in CustomerList)
            {
                Console.WriteLine(cust.CustomerID);
                Console.WriteLine(cust.FirstName + " " + cust.LastName);
                Console.WriteLine(cust.ContactNo);
            }


            Console.ReadLine();

        }
    }
}
