﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Day7Mvc.Models
{
    public class EmpDetails
    {

        public int Age { get; set; }
        public string Name { get; set; }
        public string Department { get; set; }
    }
}