﻿using System;
using Day2Training;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace Program.test
{
    [TestClass]
    public class ProgramTest
    {
        [TestMethod]
        public void TestMethod1()
        {
            Employee employee =  new Employee();
            employee.Name = "Aditya";
            employee.Age = 21;

            string expected = "Aditya, 21";

            string actual = employee.Name +", "+ employee.Age;

            Assert.AreEqual(expected, actual);

        }
    }
}
