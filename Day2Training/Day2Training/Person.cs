﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Day2Training
{
    public class Person: IPersonal
    {
        public string Name{ get; set; }
        public int Age { get; set; }
        public string Address { get; set; }
        public int number { get; set; }


        public Person()
        {

        }

        public Person(string name, int age, string address, int number)
        {
            this.Name = name;
            this.Age = age;
            this.Address = address;
            this.number = number;
        }

        public void Display()
        {
            Console.WriteLine("Name "+Name);
            Console.WriteLine("Age " +Age);
            Console.WriteLine("Address " +Address);
            Console.WriteLine("Contact Number " +number);
        }

       
    }
}
