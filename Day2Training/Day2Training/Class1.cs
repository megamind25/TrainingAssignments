﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Day2Training
{
    public class Employee : Person
    {
        public int ID { get; set; }
        public string Designation { get; set; }
        public int Salary { get; set; }
        public int Experience { get; set; }

        public Employee()
        {

        }

        public Employee(string name, int age, string address, int number, int id, string desg, int salary, int experience) : base(name, age, address, number)
        {
            this.ID = id;
            this.Designation = desg;
            this.Salary = salary;
            this.Experience = experience;

        }

        public void EmployeeDisplay()
        {
            Display();
            Console.WriteLine("Designation : " + Designation);
            Console.WriteLine("ID : " + ID);
            Console.WriteLine("Salary : "+Salary);
            Console.WriteLine("Experience: "+Experience);
        }


        public static bool operator >(Employee emp1, Employee emp2)
        {
            if (emp1.Age > emp2.Age)
                return true;
            else
                return false;
        }


        public static bool operator <(Employee emp1, Employee emp2)
        {
            if (emp1.Age < emp2.Age)
                return true;
            else
                return false;
        }

        public static bool operator ==(Employee emp1, Employee emp2)
        {
            if (emp1.Designation == emp2.Designation)
            {
                return true;
            }

            else
                return false;
        }


        public static bool operator !=(Employee emp1, Employee emp2)
        {
            if (emp1.Designation != emp2.Designation)
            {
                return true;
            }

            else
                return false;
        }




    }


    class Contractor : Person
    {

        public string LicenseNo { get; set; }
        public int Rate { get; set; }
        public string Duration { get; set; }

        public Contractor(string name, int age, string address, int number, string license, int rate, string duration) : base(name, age, address, number)
        {
            this.LicenseNo = license;
            this.Rate = rate;
            this.Duration = duration;
        }

        public void ContractorDisplay()
        {
            Display();
            Console.WriteLine("License Number : " + LicenseNo);
            Console.WriteLine("Rate : " + Rate);
            Console.WriteLine("Duration : " + Duration);
        }


    }
}
