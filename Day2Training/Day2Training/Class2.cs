﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Day2Training
{
    public static class Experience
    {
        

        public static void CheckExperience(this Employee emp)
        {
            if(emp.Experience > 5)
                Console.WriteLine(emp.Name+ " has more than 5 years experience.");
            else if(emp.Experience == 5)
                Console.WriteLine(emp.Name+ " has 5 years of experience.");
            else
                Console.WriteLine(emp.Name+ " has less tham 5 years of experience.");

        }
    }
}
