﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Day2Training
{
    class Program
    {
        static void Main(string[] args)
        {

            List<Employee> empList = new List<Employee> { };
            List<Contractor> contList = new List<Contractor> { };


            Employee person = new Employee("Aditya", 21, "Pune", 8793, 123, "SE", 450000, 1);
            empList.Add(person);
            int option;
            do
            {
                Console.WriteLine("Enter the operation to be performed: " +
               "\n 1. Input Details" +
               "\n 2. Display Details" +
               "\n 3. Compare Ages" +
               "\n 4. Compare Designations" +
               "\n 5. Check Experience" +
               "\n 6. Exit");

                 option = int.Parse(Console.ReadLine());


                switch (option)
                {
                    case 1:
                        Console.WriteLine("Enter type of job:\n 1) Full Time Employee\n 2)Contractor");

                        int type = int.Parse(Console.ReadLine());

                        if (type == 1)
                            empList.Add(GetEmployeeDetails());

                        else if (type == 2)
                            contList.Add(GetContractorDetails());

                        Console.WriteLine();
                        break;

                    case 2:
                        for (int i = 0; i < empList.Count(); i++)
                        {
                            Console.WriteLine("Employee Details\n");
                            empList[i].EmployeeDisplay();
                        }
                        Console.WriteLine();
                        break;


                    case 3:
                        compareAges(empList[0], empList[1]);
                        Console.WriteLine();
                        break;

                    case 4:
                        compareDesignations(empList[0], empList[1]);
                        Console.WriteLine();
                        break;


                    case 5:
                        empList[1].CheckExperience();
                        Console.WriteLine();
                        break;

                    case 6:
                        break;

                    default:
                        Console.WriteLine("Incorrect option");
                        Console.WriteLine();
                        break;
                }

            } while (option != 6);





            
        }



        public static Employee GetEmployeeDetails()
        {
            Employee emp = new Employee();

            Console.WriteLine("Enter name: ");
            emp.Name = Console.ReadLine();
            Console.WriteLine("Enter age: ");
            emp.Age = int.Parse(Console.ReadLine());
            Console.WriteLine("Enter address: ");
            emp.Address = Console.ReadLine();
            Console.WriteLine("Enter number");
            emp.number = int.Parse(Console.ReadLine());
            Console.WriteLine("Enter ID");
            emp.ID = int.Parse(Console.ReadLine()); 
            Console.WriteLine("Enter Designation");
            emp.Designation = Console.ReadLine();
            Console.WriteLine("Enter Salary");
            emp.Salary = int.Parse(Console.ReadLine());
            Console.WriteLine("Enter Experience");
            emp.Experience = int.Parse(Console.ReadLine());

            return emp;
        }





        public static Contractor GetContractorDetails()
        {

            Console.WriteLine("Enter name: ");
            var Name = Console.ReadLine();
            Console.WriteLine("Enter age: ");
            var Age = int.Parse(Console.ReadLine());
            Console.WriteLine("Enter address: ");
            var Address = Console.ReadLine();
            Console.WriteLine("Enter number");
            var number = int.Parse(Console.ReadLine());
            Console.WriteLine("Enter License number");
            var License = (Console.ReadLine());
            Console.WriteLine("Enter Rate");
            var Rate = int.Parse(Console.ReadLine());
            Console.WriteLine("Enter Duration");
            var Duration = (Console.ReadLine());

            Contractor cont = new Contractor(Name,Age,Address,number,License,Rate,Duration);

            return cont;
        }


        public static void compareAges(Employee emp1, Employee emp2)
        {
            if (emp1 > emp2)
                Console.WriteLine(emp1.Name + " is elder to " + emp2.Name);
            else
                Console.WriteLine(emp2.Name + " is elder to " + emp1.Name);
        }



        public static void compareDesignations(Employee emp1, Employee emp2)
        {
            if (emp1 == emp2)
                Console.WriteLine("Both employees have the same designation and pay grades.");

            else
                Console.WriteLine("Both employees have different designations and pay grades.");
        }


        

    }
}
