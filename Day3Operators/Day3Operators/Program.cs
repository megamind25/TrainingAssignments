﻿using Factorial;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Day3Operators
{
    class Program
    {
        static void Main(string[] args)
        {


            
            

            FactorialClass fc = new FactorialClass();
            fc.whereToSend += DisplayFactorial;
            fc.whereToSend += DisplayAnalysis;
            Console.WriteLine();
            fc.Calculate(10);
            Console.WriteLine("Execution");


            
            /*DummyClass dummy = new DummyClass();
            dummy.iNum = 100;
            dummy.MyFunction();

           


            Customer[] customers = new Customer[10];
            int? length = customers?.Length;
            Console.WriteLine("The length of the array is "+length);
            Customer first = customers?[0];
            Console.WriteLine("First Customer in array: "+ first?.Name);
            int? count = customers?[0]?.Orders?.Count();



            int dummyVar = 100;

            Console.WriteLine("Prefix increment" + ++dummyVar);
            Console.WriteLine("Postfix increment" + dummyVar++);
            Console.WriteLine(dummyVar);


            Console.WriteLine("Type of dummyVar is: " +typeof(int));
            Console.WriteLine("Type of customer is: "+typeof(Customer));

            Console.WriteLine("Size of dummyVar is: " + sizeof(int));

            Customer cust = new Customer();
            Console.WriteLine("Checking object: "+ (cust is Customer));
            Console.WriteLine("Checking Customer object: "+ (cust is Customer));


            object[] objVal = new object[4];
            objVal[0] = new Customer();
            objVal[1] = new Order();
            objVal[2] = 123;
            objVal[3] = "str";

            for (int i = 0; i < 4; i++)
            {
                string j = objVal[i] as string;
                Console.WriteLine(j);
            }

            */
            Console.ReadKey();
        }

        static void DisplayFactorial(int number)
        {
            Console.WriteLine("Callback: "+number);
        }

        static void DisplayAnalysis(int number)
        {
            if(number < 100)
                Console.WriteLine("The number is lesser than 100");
            else
                Console.WriteLine("The number is greater than 100");
        }
    }
}
