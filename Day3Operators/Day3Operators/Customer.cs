﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Day3Operators
{
    class Customer
    {
        public string Name { get; set; }
        public Order[] Orders { get; set; }
    }
}
