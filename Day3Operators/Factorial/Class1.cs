﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Factorial
{
    public class FactorialClass
    {

        public delegate void SendResult(int n);
        public SendResult whereToSend;

        public void Calculate(int number)
        {
            int result=1;
            for (int i = 1; i <= number; i++)
            {
                result = result * i;
            }
            whereToSend(result);
            //return result;
        }
    }
}
