﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;



namespace Day11WebAPIDemo.Models
{
    public class EmployeeViewModel
    {
        //[Key]
       // public int empID { get; set; }
        public string empFirstName { get; set; }
        public string empLastName { get; set; }
        public int empContactNo { get; set; }
        public string empEmail { get; set; }
    }
}