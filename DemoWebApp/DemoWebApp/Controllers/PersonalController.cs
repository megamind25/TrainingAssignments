﻿using DemoWebApp.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace DemoWebApp.Controllers
{
    public class PersonalController : Controller
    {
        // GET: Personal
        public ActionResult Index()
        {
            return View();
        }

        public ActionResult DisplayDetails()
        {
            List<PersonalDetails> personList = new List<PersonalDetails>();
            PersonalDetails p1 = new PersonalDetails { ID = 7, Name = "Aditya", Age = 21 };
            PersonalDetails p2 = new PersonalDetails { ID = 8, Name = "Jai", Age = 22 };
            PersonalDetails p3 = new PersonalDetails { ID = 9, Name = "Praveen", Age = 21 };

            personList.Add(p1);
            personList.Add(p2);
            personList.Add(p3);


            return View(personList);
        }

        [HttpGet]
        public ActionResult FormEntry()
        {
            return View();
        }

        [HttpPost]
        public ActionResult FormEntry(PersonalDetails person)
        {
            /*if (!ModelState.IsValid)
            {

            }*/
           
            string name = person.Name;
            return Content(name);
        }
    }
}