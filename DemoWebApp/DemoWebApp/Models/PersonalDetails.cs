﻿using DemoWebApp.CustomValidators;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace DemoWebApp.Models
{
    public class PersonalDetails //: ValidationAttribute
    {
        public int Age { get; set; }

        [ScaffoldColumn(false)]
        public int ID { get; set; }

        [Required] [MyCustomValidator(2)]
        public string Name { get; set; }


    }
}