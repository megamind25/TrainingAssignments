﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.ServiceModel;
using System.Text;
using System.Threading;

namespace Day18WcfOddEven
{
    // NOTE: You can use the "Rename" command on the "Refactor" menu to change the class name "Service1" in both code and config file together.
    [ServiceBehavior(InstanceContextMode = InstanceContextMode.PerCall,ConcurrencyMode = ConcurrencyMode.Multiple)]
    public class Service1 : IService1
    {

        int counter = 0;
        int i = 0;

        public string Call(string name)
        {
            i++;
            Thread.Sleep(5000);
            return String.Format("Client name : {0} Instance:{1} Thread: {2} Time:{3}", name, i, Thread.CurrentThread.ManagedThreadId, DateTime.Now);
            
        }

        public string CheckNumber(int value)
        {
            string ans = "";
            if (value % 2 == 0)
                ans = "even";
            else
                ans = "odd";

            return string.Format("You entered: {0}. The number is {1}", value,ans);
        }

        public int Increment()
        {
            counter++;
            return counter;
        }

        public string ProcessingTime()
        {

            DateTime dt = DateTime.Now;
            Thread.Sleep(5000);
            DateTime dt1 = DateTime.Now;
            string ptime = "The processing time was " + (dt1 - dt);
            return (ptime);
        }

        public void WaitTime(int seconds)
        {
            Thread.Sleep(seconds);
        }
    }
}
