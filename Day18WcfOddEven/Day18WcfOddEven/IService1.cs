﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.ServiceModel;
using System.Text;

namespace Day18WcfOddEven
{
    // NOTE: You can use the "Rename" command on the "Refactor" menu to change the interface name "IService1" in both code and config file together.
    [ServiceContract]
    public interface IService1
    {
        [OperationContract]
        string CheckNumber(int value);

        [OperationContract(IsOneWay =false)]
        string ProcessingTime();

        [OperationContract(IsOneWay = true)]
        void WaitTime(int seconds);


        [OperationContract]
        int Increment();

        [OperationContract]
        string Call(string name);


    }

    // Use a data contract as illustrated in the sample below to add composite types to service operations.
    // You can add XSD files into the project. After building the project, you can directly use the data types defined there, with the namespace "Day18WcfOddEven.ContractType".


    /*[DataContract]
    public class CompositeType
    {
        
        string stringValue = "Hello ";
        
        [DataMember]
        public string StringValue
        {
            get { return stringValue; }
            set { stringValue = value; }
        }
    }*/
}
