﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Day16WCFConsume
{
    class Program
    {
        static void Main(string[] args)
        {
            ServiceReferenceWCF.Service1Client client = new ServiceReferenceWCF.Service1Client();
            foreach(ServiceReferenceWCF.Employee emp in client.GetAllEmployees())
            {
                Console.WriteLine(emp.empID + " " + emp.empName);
            }

            ServiceReferenceWCF.Employee empl = client.GetEmployeeById(2);
            Console.WriteLine(empl.empID+" "+empl.empName);

            Console.ReadLine();

        }
    }
}
